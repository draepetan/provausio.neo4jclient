﻿using Xunit;

namespace Provausio.Neo4jClient.Tests
{
    public class MatchNodeBuilderTests
    {
        private readonly NodeBuilder _builder;

        public MatchNodeBuilderTests()
        {
            _builder = new NodeBuilder();
        }

        [Fact]
        public void ByNode_Default_EmptyParens()
        {
            // arrange
            var builder = _builder.ByNode();

            // act
            var result = builder.Build();

            // assert
            Assert.Equal("()", result);
        }

        [Fact]
        public void ByNode_WithAlias_AliasOnly()
        {
            // arrange
            var builder = _builder.ByNode("test");

            // act
            var result = builder.Build();

            // assert
            Assert.Equal("(test)", result);
        }

        [Fact]
        public void ByNode_WithProperties_PropertiesOnly()
        {
            // arrange
            var builder = _builder.ByNode(new { prop1 = "foo", prop2 = "bar" });

            // act
            var result = builder.Build();

            // assert
            Assert.Equal("({prop1:\"foo\",prop2:\"bar\"})", result);
        }

        [Fact]
        public void ByNode_AliasAndType_Expected()
        {
            // arrange
            var builder = _builder.ByNode("test", "testType");

            // act
            var result = builder.Build();

            // assert
            Assert.Equal("(test:testType)", result);
        }

        [Fact]
        public void ByNode_AliasAndProperties_Expected()
        {
            // arrange
            var builder = _builder.ByNode("test", new { prop1 = "foo", prop2 = "bar"});

            // act
            var result = builder.Build();

            // assert
            Assert.Equal("(test {prop1:\"foo\",prop2:\"bar\"})", result);
        }

        [Fact]
        public void ByNode_AliasTypeProperties_Expected()
        {
            // arrange
            var builder = _builder.ByNode("test", "testType", new { prop1 = "foo", prop2 = "bar" });

            // act
            var result = builder.Build();

            // assert
            Assert.Equal("(test:testType {prop1:\"foo\",prop2:\"bar\"})", result);
        }
    }
}
