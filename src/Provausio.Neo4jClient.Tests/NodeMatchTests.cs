﻿using Xunit;

namespace Provausio.Neo4jClient.Tests
{
    public class NodeMatchTests
    {
        [Fact]
        public void Match_AddsKeyword()
        {
            // arrange
            var match = new Neo4jClient.NodeMatch();

            // act
            var result = match.ToString();

            // assert
            Assert.Equal("MATCH ", result);
        }

        [Fact]
        public void Node_AddsNode()
        {
            // arrange
            var properties = new TestClass
            {
                Prop1 = "foo"
            };

            var node = new Node
            {
                Properties = properties,
                Alias = "t",
                Type = "TestClass"
            };

            var match = new Neo4jClient.NodeMatch();

            // act
            match.MatchNode(node);

            // assert
            Assert.Equal("MATCH (t:TestClass {Prop1:\"foo\"})", match.ToString());
        }

        [Fact]
        public void WithRelationship_JoinsTwoNodes()
        {
            // arrange
            var properties = new TestClass
            {
                Prop1 = "foo"
            };

            var node = new Node
            {
                Properties = properties,
                Alias = "t",
                Type = "TestClass"
            };

            var relationship = new Relationship("IMPROVES", RelationshipDirection.Right);

            var match = new Neo4jClient.NodeMatch();
            match.MatchNode(node);

            // act
            match.WithRelationshipTo(relationship, node);

            // assert
            Assert.Equal("MATCH (t:TestClass {Prop1:\"foo\"})-[:IMPROVES]->(t:TestClass {Prop1:\"foo\"})", match.ToString());
        }

        private class TestClass
        {
            public string Prop1 { get; set; }
        }
    }
}
