﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Provausio.Neo4jClient.Tests
{
    public class RelationshipTests
    {
        [Fact]
        public void Ctor_NoParams_EmptyBrackets()
        {
            // arrange
            var r = new Relationship();

            // act
            var value = r.ToString();

            // assert
            Assert.Equal("[]", value);
        }

        [Fact]
        public void Ctor_WithAlias_AliasOnly()
        {
            // arrange
            var r = new Relationship("myVar");

            // act
            var value = r.ToString();

            // assert
            Assert.Equal("[myVar]", value);
        }

        [Fact]
        public void Ctor_WithDescriptor_Right_AliasOnly()
        {
            // arrange
            var r = new Relationship("WORKS_FOR", RelationshipDirection.Right);

            // act
            var value = r.ToString();

            // assert
            Assert.Equal("-[:WORKS_FOR]->", value);
        }

        [Fact]
        public void Ctor_WithDescriptor_Left_AliasOnly()
        {
            // arrange
            var r = new Relationship("WORKS_FOR", RelationshipDirection.Left);

            // act
            var value = r.ToString();

            // assert
            Assert.Equal("<-[:WORKS_FOR]-", value);
        }

        [Fact]
        public void Ctor_WithAliasAndDescriptor_Expected()
        {
            // arrange
            var r = new Relationship("myVar", "WORKS_FOR", RelationshipDirection.Right);

            // act
            var value = r.ToString();

            // assert
            Assert.Equal("-[myVar:WORKS_FOR]->", value);
        }
    }
}
