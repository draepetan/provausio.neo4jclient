﻿namespace Provausio.Neo4jClient
{
    public enum RelationshipDirection
    {
        Unspecified,
        Right,
        Left
    }
}