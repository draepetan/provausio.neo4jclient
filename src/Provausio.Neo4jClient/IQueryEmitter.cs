﻿namespace Provausio.Neo4jClient
{
    public interface IQueryEmitter
    {
        string Build();
    }
}