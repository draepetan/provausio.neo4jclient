﻿using System;
using System.Text;

namespace Provausio.Neo4jClient
{
    public class Relationship : IQueryEmitter
    {
        public string Alias { get; }

        public string Descriptor { get; }

        public RelationshipDirection Direction { get; }

        /// <summary>
        /// Describes "any relationship"
        /// </summary>
        public Relationship()
        {
        }

        /// <summary>
        /// Descrbes "any relationship" and gives it a variable name.
        /// </summary>
        /// <param name="alias"></param>
        public Relationship(string alias)
        {
            Alias = alias;
        }

        /// <summary>
        /// Describes a directional relationship of a particular kind.
        /// </summary>
        /// <param name="descriptor"></param>
        /// <param name="direction"></param>
        public Relationship(string descriptor, RelationshipDirection direction)
        {
            Descriptor = descriptor;
            Direction = direction;
        }

        /// <inheritdoc />
        /// <summary>
        /// Describes a directional relationship of a particular kind and gives it a variable name.
        /// </summary>
        /// <param name="alias"></param>
        /// <param name="descriptor"></param>
        /// <param name="direction"></param>
        public Relationship(string alias, string descriptor, RelationshipDirection direction)
            : this(descriptor, direction)
        {
            Alias = alias;
        }

        public static Relationship Create(string descriptor, RelationshipDirection direction, string alias = null)
        {
            return new Relationship(alias, descriptor, direction);
        }

        public string Build()
        {
            var builder = new StringBuilder();

            if (!string.IsNullOrEmpty(Alias))
                builder.Append($"{Alias}");

            if (!string.IsNullOrEmpty(Descriptor))
                builder.Append($":{Descriptor}");

            string relationshipStatement;


            switch (Direction)
            {
                case RelationshipDirection.Right:
                    relationshipStatement = $"-[{builder}]->";
                    break;
                case RelationshipDirection.Left:
                    relationshipStatement = $"<-[{builder}]-";
                    break;
                case RelationshipDirection.Unspecified:
                    relationshipStatement = $"[{builder}]";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return relationshipStatement;
        }

        public override string ToString()
        {
            return Build();
        }
    }
}