﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Provausio.Neo4jClient
{
    public interface INodeMatch : IQueryEmitter
    {
        INodeMatch MatchNode(Node node);

        INodeMatch WithRelationshipTo(
            Relationship relationship,
            Node node);
    }

    public interface IFilterable
    {

    }

    public class NodeMatch : INodeMatch
    {
        private readonly List<Action<StringBuilder>> _actions;

        public NodeMatch()
        {
            _actions = new List<Action<StringBuilder>>();
        }

        public INodeMatch MatchNode(Node node)
        {
            _actions.Add(builder => builder.Append(node));
            return this;
        }

        public INodeMatch WithRelationshipTo(
            Relationship relationship, 
            Node node)
        {
            var matchStatement = $"{relationship}{node}";
            _actions.Add(builder => builder.Append(matchStatement));
            return this;
        }

        public string Build()
        {
            var builder = new StringBuilder("MATCH ");
            foreach (var step in _actions)
                step(builder);

            builder.Append(Environment.NewLine);

            return builder.ToString();
        }

        public override string ToString()
        {
            return Build();
        }
    }
}