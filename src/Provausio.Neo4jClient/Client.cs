﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Provausio.Neo4jClient
{
    public class Client
    {
        public ICypherQueryBuilder QueryBuilder { get; }

        public Client()
        {
            
        }
    }

    public interface ICypherQueryBuilder : IQueryEmitter
    {
        INodeMatch Match(Action<Node> match);

        INodeMatch Match(string alias = null, string type = null, object properties = null);
    }
    

    public class ThrowMeAway
    {
        public void Do()
        {
            var c = new Client();
            c.QueryBuilder
                .Match()
                .WithRelationshipTo(Relationship.Create("", RelationshipDirection.Right), new Node());
        }
    }
}
