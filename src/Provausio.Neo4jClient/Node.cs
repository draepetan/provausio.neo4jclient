﻿using System.IO;
using Newtonsoft.Json;

namespace Provausio.Neo4jClient
{
    public class Node
    {
        public string Type { get; set; }

        public string Alias { get; set; }

        public object Properties { get; set; }

        public override string ToString()
        {
            var builder = new NodeBuilder();
            builder.ByNode(Alias, Type, Properties);
            return builder.ToString();
        }
    }

    public static class UnquotedSerializer
    {
        public static string SerializeObject(object target)
        {
            var serializer = new JsonSerializer();
            var stringWriter = new StringWriter();
            using (var writer = new JsonTextWriter(stringWriter))
            {
                writer.QuoteName = false;
                serializer.Serialize(writer, target);
                return stringWriter.ToString();
            }
        }
    }
}
