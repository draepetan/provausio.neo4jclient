﻿using System.Text;

namespace Provausio.Neo4jClient
{

    public class NodeBuilder 
    {
        private string _alias;
        private string _type;
        private object _properties;

        public NodeBuilder ByNode()
        {
            return this;
        }

        public NodeBuilder ByNode(string alias)
        {
            _alias = alias;
            return this;
        }

        public NodeBuilder ByNode(object properties)
        {
            _properties = properties;
            return this;
        }

        public NodeBuilder ByNode(string alias, string type)
        {
            _type = type;
            return ByNode(alias);
        }

        public NodeBuilder ByNode(string alias, object properties)
        {
            _properties = properties;
            return ByNode(alias);
        }

        public NodeBuilder ByNode(string alias, string type, object properties)
        {
            _type = type;
            return ByNode(alias, properties);
        }

        public string Build()
        {
            var hasAlias = !string.IsNullOrEmpty(_alias);
            var hasType = !string.IsNullOrEmpty(_type);
            var hasProperties = _properties != null;


            var sb = new StringBuilder();
            
            if (hasAlias)
                sb.Append(_alias);

            if (hasType)
                sb.Append($":{_type}");

            if (hasProperties && (hasAlias || hasType))
                sb.Append(" ");

            if (hasProperties)
                sb.Append(UnquotedSerializer.SerializeObject(_properties));
            
            return $"({sb})";
        }

        public override string ToString()
        {
            return Build();
        }
    }
}